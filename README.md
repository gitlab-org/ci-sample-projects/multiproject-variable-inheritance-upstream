## Multiproject variables inheritance 


In this example, we are demonstrating passing variables to a downstream pipeline using [dotenv variable inheritance](https://docs.gitlab.com/ee/ci/variables/README.html#pass-an-environment-variable-to-another-job) and [cross project artifact downloads](https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs). 

**Notice:** There is no such file `credentials.env` living in both projects repositories. This is passed down dynamically as job artifacts only.

## ℹ️ Documetation links
Read more about:
1. [dotenv variable inheritance](https://docs.gitlab.com/ee/ci/variables/README.html#pass-an-environment-variable-to-another-job)
2. [Cross project artifact downloads](https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs)
3. [Pass variables to a downstream pipeline with dotenv variable inheritance and cross project artifact downloads](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#with-variable-inheritance)

